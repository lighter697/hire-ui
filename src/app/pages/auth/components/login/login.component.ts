import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthTokenService} from '../../../../core/services/auth-token/auth-token.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authTokenService: AuthTokenService,
    private router: Router,
  ) {
    this.form = this.fb.group({
      username: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', [Validators.required]),
    });
  }

  ngOnInit() {
  }

  login(event) {
    event.preventDefault();
    event.stopPropagation();
    this.authTokenService.getAuthTokenByLoginData(this.form.value).subscribe(
      value => {
        this.authTokenService.updateToken(value);
        this.form.reset();
        this.router.navigate(['/']).then();
      }
    );
  }

}
