import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ProfilesComponent} from './profiles.component';
import {ProfilesRoutingModule} from './routing/profiles-routing.module';
import {NgbDropdownModule, NgbModalModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {CreateProfileComponent} from './components/create-profile/create-profile.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ShowProfileComponent} from './components/show-profile/show-profile.component';
import {SearchComponent} from './components/search/search.component';
import {FiltersComponent} from './components/filters/filters.component';
import {Ng5SliderModule} from 'ng5-slider';
import {ClipboardModule} from 'ngx-clipboard';

@NgModule({
  entryComponents: [
    CreateProfileComponent,
    ShowProfileComponent,
    SearchComponent,
    FiltersComponent
  ],
  imports: [
    CommonModule,
    ProfilesRoutingModule,
    NgbModalModule,
    FormsModule,
    ReactiveFormsModule,
    NgbDropdownModule,
    Ng5SliderModule,
    NgbPaginationModule,
    ClipboardModule,
  ],
  declarations: [
    ProfilesComponent,
    CreateProfileComponent,
    ShowProfileComponent,
    SearchComponent,
    FiltersComponent
  ],
})
export class ProfilesModule {
}
