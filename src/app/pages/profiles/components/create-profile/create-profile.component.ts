import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ProfileService} from '../../services/profile.service';
import {take} from 'rxjs/operators';
import {ImageService} from '../../services/image.service';
import {BasicModel} from '../../../../core/model/basic.model';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})
export class CreateProfileComponent implements OnInit {
  group: FormGroup;
  imageBasePath = environment.apiDomain + '/images/avatars/';

  @Input() profile: BasicModel = {
    id: null,
    firstName: null,
    lastName: null,
    age: null,
    label: null,
    experience: null,
    experienceLastJob: null,
    email: null,
    currentJob: null,
    lastEducation: null,
    picture: {
      id: null,
      filename: null
    },
    phone: null,
    skype: null,
    summary: null,
    address: null,
    profiles: [],
    work: [],
    educations: [],
    skills: [],
    languages: [],
    references: [],
  };
  avatar: string;

  @Output() profileCreated = new EventEmitter();

  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private profileService: ProfileService,
    private imageService: ImageService,
  ) {

  }

  ngOnInit() {
    this.group = this.fb.group({
      firstName: this.fb.control(this.profile.firstName, [Validators.required]),
      lastName: this.fb.control(this.profile.lastName, [Validators.required]),
      label: this.profile.label,
      picture: this.profile.picture ? this.profile.picture.id : null,
      email: this.fb.control(this.profile.email, [Validators.email]),
      phone: this.profile.phone,
      skype: this.profile.skype,
      summary: this.profile.summary,
      address: this.profile.address,
      profiles: this.fb.array(this.profile.profiles.map(item => {
        return this.fb.group({
          network: this.fb.control(item.network, [Validators.required]),
          username: item.username,
          url: this.fb.control(item.url, [Validators.required]),
        });
      })),
      work: this.fb.array(this.profile.work.map(item => {
        return this.fb.group({
          company: this.fb.control(item.company, [Validators.required]),
          position: this.fb.control(item.position, [Validators.required]),
          website: item.website,
          startDate: item.startDate,
          endDate: item.endDate,
          summary: item.summary,
        });
      })),
      educations: this.fb.array(this.profile.educations.map(item => {
        return this.fb.group({
          school: this.fb.control(item.school, [Validators.required]),
          degree: item.degree,
          fieldOfStudy: item.fieldOfStudy,
          startDate: item.startDate,
          endDate: item.endDate,
          description: item.description,
        });
      })),
      skills: this.fb.array(this.profile.skills.map(item => {
        return this.fb.group({
          name: this.fb.control(item.name, [Validators.required]),
          level: item.level,
        });
      })),
      languages: this.fb.array(this.profile.languages.map(item => {
        return this.fb.group({
          language: this.fb.control(item.language, [Validators.required]),
          fluency: item.fluency,
        });
      })),
      references: this.fb.array(this.profile.references.map(item => {
        return this.fb.group({
          name: this.fb.control(item.name, [Validators.required]),
          reference: item.reference,
        });
      })),
    });

    if (this.profile.hasOwnProperty('picture')) {
      if (this.profile.picture.hasOwnProperty('filename')) {
        this.avatar = this.profile.picture.filename ? this.imageBasePath + this.profile.picture.filename : null;
      }
    }
  }

  get firstName() {
    return this.group.get('firstName');
  }

  get lastName() {
    return this.group.get('lastName');
  }

  get picture() {
    return this.group.get('picture');
  }

  get email() {
    return this.group.get('email');
  }

  get profiles() {
    return this.group.get('profiles') as FormArray;
  }

  get work() {
    return this.group.get('work') as FormArray;
  }

  get educations() {
    return this.group.get('educations') as FormArray;
  }

  get skills() {
    return this.group.get('skills') as FormArray;
  }

  get languages() {
    return this.group.get('languages') as FormArray;
  }

  get references() {
    return this.group.get('references') as FormArray;
  }

  addReference() {
    this.references.push(this.fb.group({
      name: this.fb.control(null, [Validators.required]),
      reference: null,
    }));
  }

  addLanguage() {
    this.languages.push(this.fb.group({
      language: this.fb.control(null, [Validators.required]),
      fluency: null,
    }));
  }

  addSkill() {
    this.skills.push(this.fb.group({
      name: this.fb.control(null, [Validators.required]),
      level: null,
    }));
  }

  addEducation() {
    this.educations.push(this.fb.group({
      school: this.fb.control(null, [Validators.required]),
      degree: null,
      fieldOfStudy: null,
      startDate: null,
      endDate: null,
      description: null,
    }));
  }

  addWork() {
    this.work.push(this.fb.group({
      company: this.fb.control(null, [Validators.required]),
      position: this.fb.control(null, [Validators.required]),
      website: null,
      startDate: null,
      endDate: null,
      summary: null,
    }));
  }

  addProfile() {
    this.profiles.push(this.fb.group({
      network: this.fb.control(null, [Validators.required]),
      username: null,
      url: this.fb.control(null, [Validators.required]),
    }));
  }

  remove(index, ref: FormArray) {
    ref.removeAt(index);
  }

  create() {
    this.profileService.create(this.group.value).pipe(
      take(1)
    ).subscribe(result => {
      this.activeModal.close();
      this.profileService.reload();
    });
  }

  update() {
    this.profileService.update(this.group.value, this.profile.id).pipe(
      take(1)
    ).subscribe(result => {
      this.activeModal.close();
      this.profileService.reload();
    });
  }

  onFilesChange(fileList: Array<File>) {
    const formData = new FormData();
    formData.append('imageFile[file]', fileList[0], fileList[0].name);
    this.imageService.upload(formData).pipe(
      take(1)
    ).subscribe((result: { id: number, url: string }) => {
      this.group.get('picture').setValue(result.id);
      this.avatar = result.url;
    }, error => {
      alert(JSON.stringify(error.error));
    });
  }

  removeImage() {
    this.avatar = null;
    this.group.get('picture').reset();
  }
}
