import {ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {CreateProfileComponent} from '../create-profile/create-profile.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FieldConfigModel} from '../../../../core/model/field-config.model';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  @Input() form: FormGroup;
  @Input() conditionsForm: FormGroup;
  @Output() submitForm: EventEmitter<boolean> = new EventEmitter<boolean>();

  subscription = new Subscription();
  formConfig: FieldConfigModel[] = [
    {
      title: 'Keywords',
      field: 'keyword',
      fieldCurrent: null,
      placeholder: 'For example: devops AND NOT developer',
    },
    {
      title: 'Companies',
      field: 'company',
      fieldCurrent: 'companyCurrent',
      placeholder: 'For example: Epam OR SoftServe',
    },
    {
      title: 'Positions',
      field: 'position',
      fieldCurrent: 'positionCurrent',
      placeholder: 'For example: PHP developer',
    },
    {
      title: 'Skills',
      field: 'skill',
      fieldCurrent: null,
      placeholder: 'For example: javascript AND nodejs',
    },
    {
      title: 'Cities',
      field: 'city',
      fieldCurrent: null,
      placeholder: 'For example: Lviv, Ukraine',
    },
    {
      title: 'Educations',
      field: 'education',
      fieldCurrent: null,
      placeholder: 'For example: Lviv, Ivan Franko',
    },
    {
      title: 'Languages',
      field: 'language',
      fieldCurrent: null,
      placeholder: 'For example: English',
    },
  ];

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private changeDetectorRef: ChangeDetectorRef,
    private modalService: NgbModal,
  ) {
  }

  ngOnInit() {
    this.subscription.add(
      this.route.queryParams.pipe(
        take(1),
      ).subscribe(queryParams => {
          this.formConfig.map(fieldConfig => {

            const group = this.fb.group({});

            // If found param in the list, add it's value to the form
            group.addControl('name', this.fb.control(fieldConfig.field));
            group.addControl('query', this.fb.control(
              queryParams.hasOwnProperty(fieldConfig.field) ? queryParams[fieldConfig.field] : null
            ));

            group.addControl('nameCurrent', this.fb.control(fieldConfig.fieldCurrent));
            group.addControl('queryCurrent', this.fb.control(
              queryParams.hasOwnProperty(fieldConfig.fieldCurrent) ? queryParams[fieldConfig.fieldCurrent] : null
            ));

            if (queryParams.hasOwnProperty(fieldConfig.field) || queryParams.hasOwnProperty(fieldConfig.fieldCurrent)) {
              this.conditions.push(group);
            }
          });
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  search() {
    this.changeDetectorRef.detectChanges();
    this.submitForm.emit(true);
  }

  change(event, control) {
    control.patchValue(event.target.checked ? 1 : null);
  }

  addField(field: FieldConfigModel) {
    this.conditions.push(this.fb.group({
      name: field.field,
      query: this.fb.control(null),

      nameCurrent: field.fieldCurrent,
      queryCurrent: this.fb.control(null),
    }));
  }

  create() {
    this.modalService.open(CreateProfileComponent, {size: 'lg'});
  }

  findFieldConfig(field) {
    return this.formConfig.find(fieldConfig => fieldConfig.field === field);
  }

  removeCondition(index) {
    this.conditions.removeAt(index);
  }

  fieldConfigFiltered() {
    return this.formConfig.filter(fieldConfig => !this.conditions.controls.some(
      control => control.get('name').value === fieldConfig.field
    ));
  }

  get conditions() {
    return this.conditionsForm.get('conditions') as FormArray;
  }
}
