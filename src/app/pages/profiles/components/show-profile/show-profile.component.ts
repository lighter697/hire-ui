import {Component, Input, OnInit} from '@angular/core';
import {BasicModel} from '../../../../core/model/basic.model';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {environment} from '../../../../../environments/environment';
import {CreateProfileComponent} from '../create-profile/create-profile.component';

@Component({
  selector: 'app-show-profile',
  templateUrl: './show-profile.component.html',
  styleUrls: ['./show-profile.component.scss']
})
export class ShowProfileComponent implements OnInit {
  @Input() profile: BasicModel;
  imageBasePath = environment.apiDomain + '/images/avatars/';

  constructor(
    public activeModal: NgbActiveModal,
    private modalService: NgbModal
  ) {
  }

  ngOnInit() {
  }

  edit(profile) {
    this.activeModal.close();
    const modalRef = this.modalService.open(CreateProfileComponent, {size: 'lg'});
    modalRef.componentInstance.profile = profile;
  }

}
