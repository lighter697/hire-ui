import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Options} from 'ng5-slider';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  @Input() form: FormGroup;
  experience: Options = {
    floor: 0,
    ceil: 10,
    step: 1,
    showTicks: true,
    enforceRange: true,
  };

  currentJob: Options = {
    floor: 0,
    ceil: 10,
    step: 1,
    showTicks: true
  };

  age: Options = {
    floor: 18,
    ceil: 50,
    step: 1,
    showTicks: true
  };

  constructor(private fb: FormBuilder, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
  }

  valueChange(value, type, options: Options) {
    this.form.setControl(type, new FormControl(value === options.floor ? null : value));
    this.submit();
  }

  highValueChange(value, type, options: Options) {
    this.form.setControl(type, new FormControl(value === options.ceil ? null : value));
    this.submit();
  }

  change(event, control) {
    this.form.setControl(control, new FormControl(event.target.checked ? 1 : null));
    this.submit();
  }

  submit() {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: this.removeEmptyAttributes(this.form.value),
    }).then();
  }

  removeEmptyAttributes(obj) {
    const newObj = {};
    Object.keys(obj).forEach(key => {
      if (obj[key] !== '') {
        newObj[key] = obj[key];
      }
    });
    return newObj;
  }
}
