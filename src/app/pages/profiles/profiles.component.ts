import {Component, OnInit} from '@angular/core';
import {combineLatest, Observable, Subscription} from 'rxjs';
import {ProfileService} from './services/profile.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {environment} from '../../../environments/environment';
import {debounceTime, delay, map, switchMap} from 'rxjs/operators';
import {ShowProfileComponent} from './components/show-profile/show-profile.component';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {AuthTokenService} from '../../core/services/auth-token/auth-token.service';
import {BasicModel} from '../../core/model/basic.model';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {
  form: FormGroup;
  conditionsForm: FormGroup;
  profiles$: Observable<BasicModel[]>;
  subscription = new Subscription();
  imageBasePath = environment.apiDomain + '/images/avatars/';
  total$: Observable<number>;
  data$: Observable<any>;

  constructor(
    private profileService: ProfileService,
    private modalService: NgbModal,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private authTokenService: AuthTokenService,
  ) {
    console.log(this.route.snapshot.queryParams['page']);
    this.form = this.fb.group({
      opened: null,
      minAge: null,
      maxAge: null,
      minExperience: null,
      maxExperience: null,
      minCurrentJob: null,
      maxCurrentJob: null,

      page: this.fb.control(this.route.snapshot.queryParams['page'] ? this.route.snapshot.queryParams['page'] : null),

      // keyword: null,
      // company: null,
      // position: null,
      // skill: null,
      // city: null,
      // education: null,
      // language: null,

      // currentCompany: null,
      // currentPosition: null,
    });

    this.conditionsForm = this.fb.group({
      conditions: this.fb.array([])
    });
  }

  ngOnInit() {

    this.data$ = combineLatest(this.route.queryParams, this.profileService.getLoad().pipe(delay(100))).pipe(
      debounceTime(100),
      switchMap(([query, load]) => this.profileService.getAll(query)),
    );

    this.profiles$ = this.data$.pipe(
      map(data => data.data)
    );

    this.total$ = this.data$.pipe(
      map(data => data.total)
    );


    this.route.queryParams.pipe();

    // Populate the form
    const subs = this.route.queryParams.subscribe(query => {
      Object.keys(query).map(key => {
        if (this.form.get(key)) {
          this.form.get(key).patchValue(query[key]);
        }
      });
    });
    this.subscription.add(subs);
  }

  show(profile) {
    const modalRef = this.modalService.open(ShowProfileComponent, {size: 'lg'});
    modalRef.componentInstance.profile = profile;
  }

  pageChange(page) {
    this.form.get('page').setValue(page === 1 ? null : page);
    this.submit();
  }

  submit() {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: this.removeEmptyAttributes({...this.form.value, ...this.transform()}),
    }).then();
  }

  search() {
    this.page.patchValue(1);
    this.submit();
  }

  removeEmptyAttributes(obj) {
    const newObj = {};
    Object.keys(obj).forEach(key => {
      if (obj[key] !== '') {
        newObj[key] = obj[key];
      }
    });
    return newObj;
  }

  logout(event) {
    event.preventDefault();
    event.stopPropagation();
    this.authTokenService.removeCurrentToken();
    this.router.navigate(['/auth', 'login']).then();
  }

  transform() {
    return this.conditions.controls.reduce((acc, current) => {
      acc[current.get('name').value] = current.get('query').value;
      if (current.get('nameCurrent').value) {
        acc[current.get('nameCurrent').value] = current.get('queryCurrent').value;
      }
      return acc;
    }, {});
  }

  joinForms() {
    return {...this.form.value, ...this.transform()};
  }

  get conditions() {
    return this.conditionsForm.get('conditions') as FormArray;
  }

  get page() {
    return this.form.get('page');
  }

}
