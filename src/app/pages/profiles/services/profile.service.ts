import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject} from 'rxjs';
import {ProfileModel} from '../../../core/model/profile.model';
import {BasicModel} from '../../../core/model/basic.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private load = new BehaviorSubject(true);

  constructor(private http: HttpClient) {
  }

  getLoad() {
    return this.load;
  }

  reload() {
    this.load.next(true);
  }

  getAll(params) {
    return this.http.get<{ data: BasicModel[], total: number }>('/profile', {params: params});
  }

  create(data) {
    return this.http.post('/profile', data);
  }

  update(data, id) {
    return this.http.post('/profile/' + id, data);
  }
}
