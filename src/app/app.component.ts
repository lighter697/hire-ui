import {Component} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LocalStorage} from '@ngx-pwa/local-storage';
import {Token} from './core/model/auth/token.model';
import {AuthTokenService} from './core/services/auth-token/auth-token.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  readonly LC_KEY = 'token';

  constructor(
    private http: HttpClient,
    private localStorage: LocalStorage,
    private authTokenService: AuthTokenService,
    private router: Router
  ) {
    this.localStorage.getItem(this.LC_KEY).pipe(
    ).subscribe((token: Token) => {
      if (token) {
        this.authTokenService.updateToken(token);
      } else {
        this.router.navigate(['/auth', 'login']).then();
      }
    });
  }
}
