import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {mergeMap, take} from 'rxjs/operators';
import {AuthTokenService} from '../core/services/auth-token/auth-token.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private tokenService: AuthTokenService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return this.tokenService.getToken().pipe(
      take(1),
      mergeMap(token => {
        if (!token) {
          return next.handle(req.clone());
        }
        let headers = req.headers || new HttpHeaders();
        if (token.tokenType && token.accessToken) {
          headers = headers.append('Authorization', `Bearer ${token.accessToken}`);
        }
        return next.handle(req.clone({headers: headers}));
      })
    );
  }
}
