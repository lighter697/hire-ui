export interface ServerTokenInterface {
  token_type: string;
  access_token: string;
  refresh_token: string;
}
