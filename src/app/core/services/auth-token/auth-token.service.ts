import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {filter, map} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {Token} from '../../model/auth/token.model';
import {LocalStorage} from '@ngx-pwa/local-storage';
import {Login} from '../../interface/auth/login.interface';
import {ServerTokenInterface} from '../../interface/auth/server-token.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthTokenService {

  readonly LC_KEY = 'token';

  private clientData = {
    client_id: environment.auth.clientId,
    client_secret: environment.auth.clientSecret,
    grant_type: environment.auth.grantType,
  };

  public token$ = new BehaviorSubject<Token>(null);

  constructor(private http: HttpClient, private localStorage: LocalStorage) {
  }

  getAuthTokenByRefreshToken(refreshToken: string) {
    const data = {...this.clientData, ...{grant_type: 'refresh_token', refresh_token: refreshToken}};
    return this.http.post<any>('/oauth/v2/token', data).pipe(
      map((token: ServerTokenInterface) => new Token(token.token_type, token.access_token, token.refresh_token)),
    );
  }

  getAuthTokenByLoginData(loginData: Login) {
    return this.http.post<any>('/oauth/v2/token', {...this.clientData, ...loginData}).pipe(
      map((token: ServerTokenInterface) => new Token(token.token_type, token.access_token, token.refresh_token)),
    );
  }

  getAuthTokenByGoogleLogin(loginData) {
    return this.http.post('/users/google', loginData).pipe(
      map((token: ServerTokenInterface) => new Token(token.token_type, token.access_token, token.refresh_token)),
    );
  }

  getAuthTokenByFacebookLogin(loginData) {
    return this.http.post('/users/facebook', loginData).pipe(
      map((token: ServerTokenInterface) => new Token(token.token_type, token.access_token, token.refresh_token)),
    );
  }

  updateToken(token: Token) {
    this.localStorage.setItemSubscribe(this.LC_KEY, token);
    this.token$.next(token);
  }

  removeCurrentToken() {
    this.removeTokenFromLC();
  }

  getToken(): BehaviorSubject<Token> {
    return this.token$;
  }

  private removeTokenFromLC() {
    this.localStorage.removeItemSubscribe(this.LC_KEY);
    this.token$.next(null);
  }
}
