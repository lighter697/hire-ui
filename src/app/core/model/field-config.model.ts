export class FieldConfigModel {
  title: string;
  field: string;
  fieldCurrent: string;
  placeholder: string;
}
