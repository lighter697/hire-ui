export class ProfileModel {
  network: string;
  username: string;
  url: string;
}
