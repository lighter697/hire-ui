export class Token {
  constructor(
    public tokenType: string,
    public accessToken: string,
    public refreshToken: string
  ) {
  }
}
