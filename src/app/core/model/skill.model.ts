import {Validators} from '@angular/forms';

export class SkillModel {
  name: string;
  level: string;
}
