export class WorkModel {
  company: string;
  position: string;
  website: string;
  startDate: string;
  endDate: string;
  summary: string;
}
