import {Validators} from '@angular/forms';

export class LanguageModel {
  language: string;
  fluency: string;
}
