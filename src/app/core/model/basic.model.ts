import {Validators} from '@angular/forms';
import {ProfileModel} from './profile.model';
import {WorkModel} from './work.model';
import {EducationModel} from './education.model';
import {SkillModel} from './skill.model';
import {LanguageModel} from './language.model';
import {ReferenceModel} from './reference.model';

export class BasicModel {
  id: number;
  firstName: string;
  lastName: string;
  age: number;
  label: string;
  picture: {
    id: number,
    filename: string,
  };
  email: string;
  experience: number;
  experienceLastJob: number;
  currentJob: WorkModel;
  lastEducation: EducationModel;
  phone: string;
  skype: string;
  summary: string;
  address: string;
  profiles: ProfileModel[];
  work: WorkModel[];
  educations: EducationModel[];
  skills: SkillModel[];
  languages: LanguageModel[];
  references: ReferenceModel[];
}
